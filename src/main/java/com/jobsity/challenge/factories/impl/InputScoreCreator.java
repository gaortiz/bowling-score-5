package com.jobsity.challenge.factories.impl;

import com.jobsity.challenge.factories.IInputScoreCreator;
import com.jobsity.challenge.model.InputScore;
import org.springframework.stereotype.Service;

@Service
public class InputScoreCreator implements IInputScoreCreator {

    public InputScore createInputScore(String name, String score) {
        return new InputScore(name, score);
    }
}
