package com.jobsity.challenge.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public interface IParserDataService {

    Map<String, List<String>> getGroupedShots(Stream<String> lines);

}
