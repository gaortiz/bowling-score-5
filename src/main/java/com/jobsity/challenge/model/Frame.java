package com.jobsity.challenge.model;

public class Frame {

    private Shot actual;
    private Shot next;
    private Shot afterNext;

    public Frame(Shot actual, Shot next, Shot afterNext) {
        this.actual = actual;
        this.next = next;
        this.afterNext = afterNext;
    }

    public Shot getActual() {
        return actual;
    }

    public Shot getNext() {
        return next;
    }

    public Shot getAfterNext() {
        return afterNext;
    }
}
